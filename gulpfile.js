let gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rig = require('gulp-rigger'),
    bs = require('browser-sync').create(),
    del = require('del'),
    ugl = require('gulp-terser'),
    imin = require('gulp-imagemin');

gulp.task('bsync', function () {
    bs.init({
        server: './dist',
        watch: true
    })
});

gulp.task('scss', function () {
    return gulp.src('app/styles/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(rig())
        .pipe(autoprefixer())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('html', function () {
    return gulp.src('app/*.html')
        .pipe(rig())
        .pipe(gulp.dest('dist/'));
})

gulp.task('js', function () {
    return gulp.src('app/js/*.js')
        .pipe(rig())
        .pipe(ugl())
        .pipe(gulp.dest('dist/js'));
})

gulp.task('img', function () {
    return gulp.src('app/img/**/*.*')
        .pipe(imin())
        .pipe(gulp.dest('dist/img/'));
})

gulp.task('fonts', function () {
    return gulp.src('app/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts/'));
})

gulp.task('font-awesome', function () {
    return gulp.src('node_modules/font-awesome/fonts/**/*.*')
        .pipe(gulp.dest('dist/fonts/'));
})

gulp.task('php', function () {
    return gulp.src('app/**/*.php')
        .pipe(gulp.dest('dist'));
});

gulp.task('build', gulp.parallel('scss', 'html', 'php', 'js', 'img', 'fonts', 'font-awesome'));
gulp.task('clean', function () {
    return del('./dist');
})

gulp.task('watch', function () {
    gulp.watch('app/styles/**/*.scss', gulp.series('scss'));
    gulp.watch('app/**/*.html', gulp.series('html'));
    gulp.watch('app/js/**/*.js', gulp.series('js'));
    gulp.watch('app/img/**/*.*', gulp.series('img'));
})

gulp.task('default', gulp.series('clean', 'build', gulp.parallel('bsync', 'watch')))



